class Movies{
    constructor(id,nom, prenom, desc, annee){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom; 
        this.desc = desc; 
        this.annee = annee;
    }
}

const data = [   
    new Movies( 0, "Steve ","Oedekerk","Ca fait beaucoup de noix ça",2002),
    new Movies( 1, "Bill","Hader","Flint Lockwood, habitant de la ville Swallow Falls, située sur une île dans l'Atlantique et vivant presque exclusivement de la pêche à la sardine, est un inventeur raté qui est la risée de la ville.",2009),
    new Movies( 2, "Kate","Winslet","When their relationship turns sour, a couple undergoes a medical procedure to have each other erased from their memories. ",2004), 
    new Movies( 3, "Bill","Oedekerk","Ca fait beaucoup de noix ça",2002),
    new Movies( 4, "Bill","Winslet","Ca fait beaucoup de noix ça",2002),
    new Movies( 5, "Bill","Hader","Ca fait beaucoup de noix ça",2002),
    new Movies( 6, "Bill","Winslet","Ca fait beaucoup de noix ça",2002),
    new Movies( 7, "Bill","Oedekerk","Ca fait beaucoup de noix ça",2002),
];

exports.getMovieById = function(id){
   return data[id]; 
}

exports.addMovie = function(dat){
    let id = data.length;
    data.push(new Movies(id, dat.nom, dat.prenom, dat.description, dat.annee))
    return true;
}

exports.allMovies = data;