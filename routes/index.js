var express = require('express');
var router = express.Router();
let movies = require("../custom_modules/movies.js")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Mes films', description: "Une liste absurde de l'ensemble de mes films" });
});

router.get('/movies', function(req, res, next){
  res.render('movies',{ title: 'La liste', description: "La liste absurde de l'ensemble de mes films", movies: movies.allMovies } )
})

router.get('/movies/:id',function(req,res,next){
  res.render('movie',{movie:movies.getMovieById(req.params.id)})
})

router.get('/movie/add', function(req,res,next){
  res.render('add');
})

router.post('/movie/add', function(req,res,next){
  let film = movies.addMovie(req.body)
  if(film){
    res.render('movies',{ title: 'La liste', description: "La liste absurde de l'ensemble de mes films", movies: movies.allMovies } )
  }
})

module.exports = router;
